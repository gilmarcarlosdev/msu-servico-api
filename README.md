# Consumidor de Microsoft security update #
## Desafio SEPLAG/COTEC para vaga de desenvolvedor Java #

Solução com caso de uso no cosumo de API de microserviços, com rotinas em backgroound para realização de requisições em Api Rest, e persistência de
dados em banco.

### Requisitos do teste ###

1 - Criar serviço, em Java, que consuma todos os "Microsoft Security Updates" disponíveis da API da microsoft e persistir todos os dados coletados no banco de dados PostgreSQL.

2 - Criar uma Timer/Schedule que fique a cada 5 minutos atualizando os dados acima, adicionando novos updates em background.

3 - Criar uma interface visual, em angular 11, onde seja possível visualizar os dados adquiridos anteriormente, onde tenha a possibilidade de filtrar pelos campos de cada "Microsoft Security Updates" .

### Infraestrutura e tecnologias utilizadas ###

* JAVA 11 
* SPRING BOOT V4+ | JPA HIBERNATE 
* POSTGRESQL 13
* ANGULAR 11

### Instruções para rodar o projeto ###

* credencias de acesso ao db, nome da base de dados e outras configurações 
* pertinentes estão no arquivo application.properties
