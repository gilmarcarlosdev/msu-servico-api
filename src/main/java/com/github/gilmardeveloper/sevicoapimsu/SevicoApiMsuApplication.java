package com.github.gilmardeveloper.sevicoapimsu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class SevicoApiMsuApplication {

	public static void main(String[] args) {
		SpringApplication.run(SevicoApiMsuApplication.class, args);
	}


}
