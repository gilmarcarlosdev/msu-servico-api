package com.github.gilmardeveloper.sevicoapimsu.msu.controles;

import com.github.gilmardeveloper.sevicoapimsu.msu.modelos.Update;
import com.github.gilmardeveloper.sevicoapimsu.msu.respostas.UpdateResponse;
import com.github.gilmardeveloper.sevicoapimsu.msu.servicos.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ApiControle {

    private static final int MAXIMO_PAGES = 10;
    @Autowired
    private UpdateService service;

    @GetMapping("/")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> index(){
        return ResponseEntity.ok().body( service.converterParaDto( service.listarTudo() ) );
    }

    @GetMapping("/id/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorId(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPor(campo) ) );
    }

    @GetMapping("/alias/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorAlias(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPorAlias(campo) ) );
    }

    @GetMapping("/document-title/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorTitle(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPorTitle(campo) ) );
    }

    @GetMapping("/severity/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorSeverity(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPorSeverity(campo) ) );
    }

    @GetMapping("/initial-release-date/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorReleaseDate(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPorReleaseDate(campo) ) );
    }

    @GetMapping("/current-release-date/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorCurrentDate(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPorCurrentDate(campo) ) );
    }

    @GetMapping("/cvrfurl/{campo}")
    public @ResponseBody
    ResponseEntity<List<UpdateResponse>> buscarPorCvrfurl(@PathVariable("campo") String campo ){
        return ResponseEntity.ok().body(service.converterParaDto( service.buscarPorCvrfurl(campo) ) );
    }
    
    @GetMapping("/page/{pagina}")
    public @ResponseBody
    ResponseEntity<Page<Update>> buscarTodosPaginado(@PathVariable("pagina") Integer pagina){
        return ResponseEntity.ok().body( service.listarTodos( PageRequest.of(pagina, MAXIMO_PAGES) ) );
    }

}
