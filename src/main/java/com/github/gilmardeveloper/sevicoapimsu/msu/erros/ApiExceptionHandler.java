package com.github.gilmardeveloper.sevicoapimsu.msu.erros;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public String SQLIntegrityConstraintViolation(Throwable error){
        System.err.println(error.getMessage());
        return "redirect:/";
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public String ConstraintViolation(Throwable error){
        System.err.println(error.getMessage());
        return "redirect:/";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public String IllegalArgumentException(Throwable error){
        System.err.println(error.getMessage());
        return "redirect:/";
    }

}
