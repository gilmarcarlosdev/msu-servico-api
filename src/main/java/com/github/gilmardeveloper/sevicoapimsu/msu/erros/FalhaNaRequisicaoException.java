package com.github.gilmardeveloper.sevicoapimsu.msu.erros;

public class FalhaNaRequisicaoException extends RuntimeException{

    public FalhaNaRequisicaoException(String message) {
        super(message);
    }
}
