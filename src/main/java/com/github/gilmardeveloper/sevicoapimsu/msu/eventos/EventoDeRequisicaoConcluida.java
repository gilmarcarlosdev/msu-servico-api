package com.github.gilmardeveloper.sevicoapimsu.msu.eventos;

import com.github.gilmardeveloper.sevicoapimsu.msu.respostas.ModeloBaseDeResposta;
import org.springframework.context.ApplicationEvent;

public class EventoDeRequisicaoConcluida extends ApplicationEvent {

    private ModeloBaseDeResposta modeloBaseDeResposta;

    public EventoDeRequisicaoConcluida(ModeloBaseDeResposta modeloBaseDeResposta ) {
        super( modeloBaseDeResposta );
        this.modeloBaseDeResposta = modeloBaseDeResposta;
    }

    public ModeloBaseDeResposta getModeloBaseDeResposta() {
        return modeloBaseDeResposta;
    }
}
