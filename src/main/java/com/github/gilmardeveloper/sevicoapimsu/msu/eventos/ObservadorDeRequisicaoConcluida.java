package com.github.gilmardeveloper.sevicoapimsu.msu.eventos;

import com.github.gilmardeveloper.sevicoapimsu.msu.modelos.Update;
import com.github.gilmardeveloper.sevicoapimsu.msu.respostas.UpdateResponse;
import com.github.gilmardeveloper.sevicoapimsu.msu.servicos.UpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ObservadorDeRequisicaoConcluida implements ApplicationListener<EventoDeRequisicaoConcluida> {

    Logger logger = LoggerFactory.getLogger(ObservadorDeRequisicaoConcluida.class);

    @Autowired
    private UpdateService updateService;

    @Override
    public void onApplicationEvent(EventoDeRequisicaoConcluida eventoDeRequisicaoConcluida) {
        List<Update> updates = converterParaListaDeEntidade(eventoDeRequisicaoConcluida
                .getModeloBaseDeResposta()
                .getUpdates());
        sincronizarNoBanco(updates);
        logger.warn("tratamento da requisição em segundo plano em andamento...");
    }

    public List<Update> converterParaListaDeEntidade(List<UpdateResponse> updateResponses){
        return updateService.converterDeDto(updateResponses);
    }

    @Async
    public void sincronizarNoBanco(List<Update> updates){
        try{
            updateService.filtraLista( updates );
        }catch (Exception exception){
            System.err.println(exception.getMessage());
        }
    }
}
