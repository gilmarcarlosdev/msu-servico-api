package com.github.gilmardeveloper.sevicoapimsu.msu.modelos;

import org.springframework.http.ResponseEntity;

public interface TarefasDeRequisicao<T> {

    ResponseEntity<T> executar();

}
