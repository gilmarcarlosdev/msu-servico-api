package com.github.gilmardeveloper.sevicoapimsu.msu.modelos;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity @Table(name = "updates", uniqueConstraints = @UniqueConstraint(columnNames = "updateId"))
public class Update {

   @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String updateId;
   private String alias;
   private String documentTitle;
   private String severity;
   private LocalDateTime initialReleaseDate;
   private LocalDateTime currentReleaseDate;
   private String cvrfUrl;

   public Update() {}

   public Update(String updateId, String alias, String documentTitle, String severity, LocalDateTime initialReleaseDate, LocalDateTime currentReleaseDate, String cvrfUrl) {
      this.updateId = updateId;
      this.alias = alias;
      this.documentTitle = documentTitle;
      this.severity = severity;
      this.initialReleaseDate = initialReleaseDate;
      this.currentReleaseDate = currentReleaseDate;
      this.cvrfUrl = cvrfUrl;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getUpdateId() {
      return updateId;
   }

   public void setUpdateId(String updateId) {
      this.updateId = updateId;
   }

   public String getAlias() {
      return alias;
   }

   public void setAlias(String alias) {
      this.alias = alias;
   }

   public String getDocumentTitle() {
      return documentTitle;
   }

   public void setDocumentTitle(String documentTitle) {
      this.documentTitle = documentTitle;
   }

   public String getSeverity() {
      return severity;
   }

   public void setSeverity(String severity) {
      this.severity = severity;
   }

   public LocalDateTime getInitialReleaseDate() {
      return initialReleaseDate;
   }

   public void setInitialReleaseDate(LocalDateTime initialReleaseDate) {
      this.initialReleaseDate = initialReleaseDate;
   }

   public LocalDateTime getCurrentReleaseDate() {
      return currentReleaseDate;
   }

   public void setCurrentReleaseDate(LocalDateTime currentReleaseDate) {
      this.currentReleaseDate = currentReleaseDate;
   }

   public String getCvrfUrl() {
      return cvrfUrl;
   }

   public void setCvrfUrl(String cvrfUrl) {
      this.cvrfUrl = cvrfUrl;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Update updateMsu = (Update) o;
      return updateId.equals(updateMsu.updateId);
   }

   @Override
   public int hashCode() {
      return updateId.hashCode();
   }

   @Override
   public String toString() {
      return "UpdateMsu{" +
              "id='" + updateId + '\'' +
              ", alias='" + alias + '\'' +
              ", documentTitle='" + documentTitle + '\'' +
              ", severity='" + severity + '\'' +
              ", initialReleaseDate=" + initialReleaseDate +
              ", currentReleaseDate=" + currentReleaseDate +
              ", cvrfUrl='" + cvrfUrl + '\'' +
              '}';
   }
}
