package com.github.gilmardeveloper.sevicoapimsu.msu.repositorios;

import com.github.gilmardeveloper.sevicoapimsu.msu.modelos.Update;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UpdateRepository extends JpaRepository<Update, Long> {

    boolean existsUpdateByUpdateId(String updateId);

    List<Update> findByUpdateIdContaining(String updateId);

    List<Update> findBycvrfUrlContaining(String campo);

    List<Update> findByCurrentReleaseDateContaining(String campo);

    List<Update> findByInitialReleaseDateContaining(String campo);

    List<Update> findBySeverityContaining(String campo);

    List<Update> findByDocumentTitleContaining(String campo);

    List<Update> findByAliasContaining(String campo);

    List<Update> findByAlias(String campo, Sort sort);

    Page<Update> findByAliasEndingWith(String campo, Pageable pageable);
}
