package com.github.gilmardeveloper.sevicoapimsu.msu.requisicoes;

import com.github.gilmardeveloper.sevicoapimsu.msu.eventos.ObservadorDeRequisicaoConcluida;
import com.github.gilmardeveloper.sevicoapimsu.msu.respostas.ModeloBaseDeResposta;
import com.github.gilmardeveloper.sevicoapimsu.msu.modelos.TarefasDeRequisicao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UpdateRequest implements TarefasDeRequisicao<ModeloBaseDeResposta> {

    Logger logger = LoggerFactory.getLogger(UpdateRequest.class);

    private final static String URI_MSU_BASE = "https://api.msrc.microsoft.com/cvrf/v2.0";
    private final static String UPDATE_ENDPOINT = "/updates";

    public ResponseEntity<ModeloBaseDeResposta> requisicaoGet(){
        logger.warn("requisição para o endpoint em andamento ...");
         return new RestTemplate()
                 .getForEntity(URI_MSU_BASE + UPDATE_ENDPOINT, ModeloBaseDeResposta.class);
    }

    @Override
    public ResponseEntity<ModeloBaseDeResposta> executar() {
        return requisicaoGet();
    }
}
