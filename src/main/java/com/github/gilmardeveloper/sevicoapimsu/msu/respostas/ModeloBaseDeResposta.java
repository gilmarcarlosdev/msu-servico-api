package com.github.gilmardeveloper.sevicoapimsu.msu.respostas;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ModeloBaseDeResposta {

    @JsonProperty("@odata.context")
    private String oDataContext;
    @JsonProperty("value")
    private List<UpdateResponse> updates;

    public String getoDataContext() {
        return oDataContext;
    }

    public void setoDataContext(String oDataContext) {
        this.oDataContext = oDataContext;
    }

    public List<UpdateResponse> getUpdates() {
        return updates;
    }

    public void setUpdates(List<UpdateResponse> updates) {
        this.updates = updates;
    }
}
