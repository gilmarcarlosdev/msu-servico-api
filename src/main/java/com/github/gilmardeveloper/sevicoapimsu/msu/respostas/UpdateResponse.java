package com.github.gilmardeveloper.sevicoapimsu.msu.respostas;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateResponse {

   @JsonProperty("ID")
   private String id;
   @JsonProperty("Alias")
   private String alias;
   @JsonProperty("DocumentTitle")
   private String documentTitle;
   @JsonProperty("Severity")
   private String severity;
   @JsonProperty("InitialReleaseDate")
   private String initialReleaseDate;
   @JsonProperty("CurrentReleaseDate")
   private String currentReleaseDate;
   @JsonProperty("CvrfUrl")
   private String cvrfUrl;

    public UpdateResponse(){}

    public UpdateResponse(String id, String alias, String documentTitle, String severity, String initialReleaseDate, String currentReleaseDate, String cvrfUrl) {
        this.id = id;
        this.alias = alias;
        this.documentTitle = documentTitle;
        this.severity = severity;
        this.initialReleaseDate = initialReleaseDate;
        this.currentReleaseDate = currentReleaseDate;
        this.cvrfUrl = cvrfUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getInitialReleaseDate() {
        return initialReleaseDate;
    }

    public void setInitialReleaseDate(String initialReleaseDate) {
        this.initialReleaseDate = initialReleaseDate;
    }

    public String getCurrentReleaseDate() {
        return currentReleaseDate;
    }

    public void setCurrentReleaseDate(String currentReleaseDate) {
        this.currentReleaseDate = currentReleaseDate;
    }

    public String getCvrfUrl() {
        return cvrfUrl;
    }

    public void setCvrfUrl(String cvrfUrl) {
        this.cvrfUrl = cvrfUrl;
    }
}
