package com.github.gilmardeveloper.sevicoapimsu.msu.servicos;

import com.github.gilmardeveloper.sevicoapimsu.msu.modelos.Update;
import com.github.gilmardeveloper.sevicoapimsu.msu.repositorios.UpdateRepository;
import com.github.gilmardeveloper.sevicoapimsu.msu.respostas.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UpdateService {

    @Autowired
    private UpdateRepository repository;

    public void salvar(List<Update> updates){
        repository.saveAll(updates);
    }

    public List<Update> buscarPor(String campo){
        return repository.findByUpdateIdContaining(campo);
    }

    public void filtraLista(List<Update> updates){
        List<Update> atualizados = updates.stream().filter( update -> repository.existsUpdateByUpdateId(update.getUpdateId()) == false ).collect(Collectors.toList());
        if(!atualizados.isEmpty()){
            salvar(atualizados);
        }
    }

    public List<Update> listarTudo(){
        return repository.findAll();
    }

    public List<UpdateResponse> converterParaDto(List<Update> updates ){
        return updates.stream().map( update -> new UpdateResponse(
                update.getUpdateId(),
                update.getAlias(),
                update.getDocumentTitle(),
                update.getSeverity(),
                LocalDateTime.from(update.getInitialReleaseDate()).format(DateTimeFormatter.ISO_DATE_TIME),
                LocalDateTime.from(update.getCurrentReleaseDate()).format(DateTimeFormatter.ISO_DATE_TIME),
                update.getCvrfUrl()
        ) ).collect(Collectors.toList());
    }

    public List<Update> converterDeDto(List<UpdateResponse> responses ){
        return responses.stream().map(response -> new Update(
                response.getId(),
                response.getAlias(),
                response.getDocumentTitle(),
                response.getSeverity(),
                LocalDateTime.parse(response.getInitialReleaseDate(), DateTimeFormatter.ISO_DATE_TIME),
                LocalDateTime.parse(response.getCurrentReleaseDate(), DateTimeFormatter.ISO_DATE_TIME),
                response.getCvrfUrl()
        )).collect(Collectors.toList());
    }

    public List<Update> buscarPorAlias(String campo) {
        return repository.findByAliasContaining(campo);
    }

    public List<Update> buscarPorTitle(String campo) {
        return repository.findByDocumentTitleContaining(campo);
    }


    public List<Update> buscarPorSeverity(String campo) {
        return repository.findBySeverityContaining(campo);
    }

    public List<Update> buscarPorReleaseDate(String campo) {
        return repository.findByInitialReleaseDateContaining(campo);
    }

    public List<Update> buscarPorCurrentDate(String campo) {
        return repository.findByCurrentReleaseDateContaining(campo);
    }

    public List<Update> buscarPorCvrfurl(String campo) {
        return repository.findBycvrfUrlContaining(campo);
    }

    public Page<Update> listarTodos(PageRequest pagina) {
        return repository.findAll(Pageable.ofSize( pagina.getPageNumber() ));
    }
}
