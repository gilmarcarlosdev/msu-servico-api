package com.github.gilmardeveloper.sevicoapimsu.msu.tarefas;

import com.github.gilmardeveloper.sevicoapimsu.msu.erros.FalhaNaRequisicaoException;
import com.github.gilmardeveloper.sevicoapimsu.msu.eventos.EventoDeRequisicaoConcluida;
import com.github.gilmardeveloper.sevicoapimsu.msu.eventos.ObservadorDeRequisicaoConcluida;
import com.github.gilmardeveloper.sevicoapimsu.msu.requisicoes.UpdateRequest;
import com.github.gilmardeveloper.sevicoapimsu.msu.respostas.ModeloBaseDeResposta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

@Component
public class AgendadorDeTarefas {

    Logger logger = LoggerFactory.getLogger(AgendadorDeTarefas.class);

    @Autowired
    private ScheduledExecutorService executorDeAgendamentos;
    @Autowired
    private UpdateRequest requisicaoUpdatesMsu;
    @Autowired
    private ApplicationEventPublisher publicadorDeEventos;

    @PostConstruct
    @Scheduled(fixedDelay = 300000, initialDelay = 300000)
    public void requisicaoDeUpdatesMsuComPeriodoFixo() {
        try {
            logger.info("execução de tarefas agendadas foi iniciado...");
            executarAgendamentoProgramado(new TarefasEmSegundoPlano(requisicaoUpdatesMsu));
        }catch (ExecutionException | InterruptedException exception){
            exception.printStackTrace();
        }
    }

    private void executarAgendamentoProgramado(Callable<ResponseEntity> tarefasEmSegundoPlano ) throws ExecutionException, InterruptedException {
        ScheduledFuture<ResponseEntity> tarefaAgendada = executorDeAgendamentos
                .schedule( tarefasEmSegundoPlano, 1, TimeUnit.SECONDS );
        ResponseEntity tarefaConcluida = tarefaAgendada.get();
        if( tarefaAgendada.isDone() ){
            verificarResultadoDaTarefa( tarefaConcluida );
        }
    }

    private void verificarResultadoDaTarefa(ResponseEntity tarefaConcluida){
        if( tarefaConcluida.getStatusCodeValue() != 200 ){
            throw new FalhaNaRequisicaoException("falha na requisição com código HTTP: "
                    + tarefaConcluida.getStatusCodeValue() + " mensagem: " + tarefaConcluida.getBody() );
        }
        publicarEventoDeTarefaRealizada( (ModeloBaseDeResposta) tarefaConcluida.getBody() );
    }

    private void publicarEventoDeTarefaRealizada(ModeloBaseDeResposta modeloBaseDeResposta ){
        publicadorDeEventos.publishEvent( new EventoDeRequisicaoConcluida( modeloBaseDeResposta ) );
    }

}
