package com.github.gilmardeveloper.sevicoapimsu.msu.tarefas;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
public class ExecutorScheduleServiceConfig {

    @Bean
    public ScheduledExecutorService scheduleService(){
        return Executors.newScheduledThreadPool(2);
    }

}
