package com.github.gilmardeveloper.sevicoapimsu.msu.tarefas;

import com.github.gilmardeveloper.sevicoapimsu.msu.modelos.TarefasDeRequisicao;
import org.springframework.http.ResponseEntity;

import java.util.concurrent.Callable;

public class TarefasEmSegundoPlano implements Callable<ResponseEntity> {

    private TarefasDeRequisicao tarefasDeRequisicao;
    private ResponseEntity executando;

    public TarefasEmSegundoPlano(TarefasDeRequisicao tarefasDeRequisicao){
        this.tarefasDeRequisicao = tarefasDeRequisicao;
    }

    @Override
    public ResponseEntity call() throws Exception {
        return tarefasDeRequisicao.executar();
    }
}
